package com.bdd.cucumber;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Slf4j
public class TestContainerWebDriverTest {
    private final String downloadDir = "/home/seluser/Downloads/", fileName = "civic_renewal_forms.zip";
    private BrowserWebDriverContainer<?> webDriverContainer;
    private RemoteWebDriver driver;

    @BeforeClass
    void setUp() {
        long memory = 32l * 1024l * 1024l;
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--window-size=1920,1080");
        webDriverContainer = new BrowserWebDriverContainer<>().withCapabilities(chromeOptions)
                .withFileSystemBind(System.getProperty("user.home") + "\\config.toml", "/opt/selenium/config.toml", BindMode.READ_ONLY);
        webDriverContainer.start();
        driver = new RemoteWebDriver(webDriverContainer.getSeleniumAddress(), chromeOptions);
    }

    @AfterClass(alwaysRun = true)
    void tearDown() {
        driver.close();
        webDriverContainer.close();
    }

    @Test
    public void openPageTest() throws Exception {
        driver.get("https://myjob.page/tools/test-files");
        driver.findElement(By.xpath("//a[text()='1MB']")).click();
        webDriverContainer.execInContainer("sh", "-c", String.format("(ls %s >> /dev/null && echo -n true) || echo -n false", downloadDir + fileName));
        webDriverContainer.execInContainer("sh", "-c", "rm", downloadDir, "*");
//        webDriverContainer.copyFileFromContainer(downloadDir + fileName, System.getProperty("user.dir") + "/" + fileName);
        log.info("Browser version is {}", driver.getCapabilities().getCapability("browserVersion"));
        log.info("Current url is {}", driver.getCurrentUrl());
    }
}